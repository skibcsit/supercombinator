import org.scalatest.FlatSpec

import lambda.lexer.LambdaLexer
import lambda.parser.LambdaParser
import gmachine.parser.InstructionsParser
import gmachine.machine.Machine
import lambda.supercombinator.SuperCombinator
import lambda.compiler.Compiler
import gmachine.machine.DiffWithErr

object Runner {

  def run(code: String): Either[String, String] = {
    LambdaLexer
      .Parse(code)
      .flatMap(
        tokens =>
          LambdaParser
            .Parse(tokens)
            .flatMap(value => {
              val sp = SuperCombinator.LambdaLifting(value)
              Machine.run(Compiler.Compile(sp), true) match {
                case DiffWithErr(_, None, res) =>
                  Either.cond(res.isDefined, res.get, "NoResult")
                case DiffWithErr(_, err, _) =>
                  Either.cond(err.isDefined, err.get, "Empty Err")
              }
            })
      )
  }

}

class GmachineSpec extends FlatSpec {

  "Gmachine" should "calculate arithmetic" in {
    assert(Runner.run("+ 333 667") == Right[String, String]("1000"))
  }
  it should "calculate nested arithmetic" in {
    assert(Runner.run("+ 2 (* 2 2)") == Right[String, String]("6"))
  }
  it should "calculate lambdas" in {
    assert(Runner.run("(\\x -> * x x) 10") == Right[String, String]("100"))
  }
  it should "calculate HOF" in {
    assert(
      Runner
        .run("(\\f -> \\x -> f (f (f (f (f x))))) (\\x -> + 1 x) 0 ") == Right[
        String,
        String
      ]("5")
    )
  }
  it should "calculate factorial" in {
    assert(
      Runner.run(
        "letrec facdec = \\n -> * n (fac (- n 1)), fac = \\n -> IF (> n 0) (facdec n) 1 in fac 7"
      ) == Right[String, String]("5040")
    )
  }
  it should "be lazy" in {
    assert(
      Runner.run(
        "letrec rec = \\n -> + 1 (rec n), k = \\x -> \\y -> x in k 1 (rec 1)"
      ) == Right[String, String]("1")
    )
  }
  it should "calculate fib" in {
    assert(Runner.run("""
        letrec 
          fibSum = \n -> + (fib (- n 1)) (fib (- n 2)),

        fib = \n ->
          IF (== 0 n)
              0
              (IF (== 1 n)
                  1
                  (fibSum n)
              )
        in fib 10
""") == Right[String, String]("55"))
  }
}
